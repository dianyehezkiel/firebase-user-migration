import admin from "firebase-admin";
import { UserImportOptions, UserImportRecord } from "firebase-admin/lib/auth/user-import-builder";
import { v4 as uuid } from "uuid";

let appSource = admin.initializeApp({
  credential: admin.credential.cert('./.serviceAccount.source.json')
}, "source");
let appTarget = admin.initializeApp({
  credential: admin.credential.cert('./.serviceAccount.target.json')
}, "target");

const sourceAuth = appSource.auth().tenantManager().authForTenant('<SOURCE_TENANT_ID>');
// const sourceAuth = appSource.auth(); // use this if exporting from firebase
const targetAuth = appTarget.auth().tenantManager().authForTenant('<TARGET_TENANT_ID>');

const importOptions: UserImportOptions = {
  hash: {
    algorithm: "SCRYPT",
    key: Buffer.from('<HASH_KEY>', 'base64'),
    saltSeparator: Buffer.from('<SALT_SEPARATOR>', "base64"),
    rounds: 8, // USUALLY SAME BUT SHOULD BE CHANGED IF DIFFERENT
    memoryCost: 14, // USUALLY SAME BUT SHOULD BE CHANGED IF DIFFERENT
  }
}

function migrateUsers(userImportOptions?: UserImportOptions, nextPageToken?: string) {
  let pageToken: string | undefined;
  sourceAuth.listUsers(1000, nextPageToken)
    .then(function (listUsersResult) {
      const users: UserImportRecord[] = [];
      listUsersResult.users.forEach(function (user) {
        var modifiedUser = user.toJSON() as UserImportRecord;
        // Convert to bytes.
        if (user.passwordHash && user.passwordSalt) {
          modifiedUser.passwordHash = Buffer.from(user.passwordHash, 'base64');
          modifiedUser.passwordSalt = Buffer.from(user.passwordSalt, 'base64');
        }
        // Delete tenant ID if available. This will be set automatically.
        modifiedUser.uid = uuid();
        delete modifiedUser.tenantId;
        delete modifiedUser.providerData;
        users.push(modifiedUser);
      });
      console.log('TOTAL EXPORT: ', users.length)
      // Save next page token.
      pageToken = listUsersResult.pageToken;
      // Upload current chunk.
      return targetAuth.importUsers(users, userImportOptions);
    })
    .then(function (results) {
      results.errors.forEach(function (indexedError) {
        console.log('Error importing users:', indexedError.error);
        console.log('Error importing user ' + indexedError.index);
      });
      // Continue if there is another page.
      if (pageToken) {
        migrateUsers(userImportOptions, pageToken);
      }
    })
    .catch(function (error) {
      console.log('Error importing users:', error);
    });
}

migrateUsers(importOptions);